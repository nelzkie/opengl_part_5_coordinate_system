
#include <iostream>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>



/************
The tutorial page

http://learnopengl.com/#!Getting-started/Hello-Window

**************/



// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;

// The MAIN function, from here we start the application and run the game loop
int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);	// since our focus on using  opengl3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // since our focus on using  opengl3.3


	/****
	Telling GLFW explicitly that we want to use the core-profile will result in invalid operation errors whenever we call one of OpenGL's legacy functions,
	which is a nice reminder when we accidentally use old functionality where we'd rather stay away from.
	****/
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);	//disable resizing



	/*****************************
	The function returns a GLFWwindow object that we'll later need for other GLFW operations.

	****************************/
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", nullptr, nullptr);	// create a window with title "LearOpengl"


	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);


	glewExperimental = GL_TRUE;		// This means that we are going to use the modern technique in opengl
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}


	/******
	1st and 2nd parameter are the x and y coordinate. in our case 0,0 means bottom left.

	3rd and 4th parameter are the size of the rendering which is the size of the GLFW window
	
	*********/
	glViewport(0, 0, WIDTH, HEIGHT);	// tell the opengl the size of the rendering window



	// meaing of callback and with example http://stackoverflow.com/questions/9596276/how-to-explain-callbacks-in-plain-english-how-are-they-different-from-calling-o
	glfwSetKeyCallback(window, key_callback);	// register the fucntion with the proper callback via GLFW


	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();	// check if any events are triggered like pressing keys or mouse movements

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// do some rendering commands here




		glfwSwapBuffers(window);	// swap the front and back buffer
	}

	glfwTerminate();	// As soon as we exit the game loop we would like to properly clean/delete all resources that were allocated.



	return 0;
}


/************

NOTE:
	KeyCallback function is a fucntion of GLFW  which should be called whenever the user interacts with the keyboard.

	The key input function takes a GLFWwindow as its first argument, an integer that specifies the key pressed, 
	an action that specifies if the key is pressed or released and an integer representing some bit flags to tell you if shift, 
	control, alt or super keys have been pressed. Whenever a user pressed a key, GLFW calls this function and fills in the proper arguments for you to process.

************/

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	// When a user presses the escape key, we set the WindowShouldClose property to true, 
	// closing the application
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)	// check if the action is pressed and not release and if the key is the esc button
		glfwSetWindowShouldClose(window, GL_TRUE);	// close the window

}




