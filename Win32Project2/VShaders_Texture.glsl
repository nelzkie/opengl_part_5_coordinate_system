

#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec2 texCoord;

out vec3 ourColor;
out vec2 TexCoord;

void main()
{
	gl_Position = vec4(position, 1.0f);
	ourColor = color;
	// We swap the y-axis by substracing our coordinates from 1. This is done because most images have the top y-axis inversed with OpenGL's top y-axis.
	// TexCoord = texCoord;


	/*************
	ISSUES:
		You probably noticed that the texture is flipped upside-down! 
		This happens because OpenGL expects the 0.0 coordinate on the y-axis to be on the bottom side of the image,
		 but images usually have 0.0 at the top of the y-axis. Some image loaders like DevIL have options to reposition the y-origin during loading, 
		 but SOIL doesn't. SOIL does have a function called SOIL_load_OGL_texture that loads and generates a texture with a flag called SOIL_FLAG_INVERT_Y 
		 that solves our problem. This function does however use features that are not available in modern OpenGL so we'd have to stick with SOIL_load_image 
		 for now and do the texture generation ourselves. To fix this with our vertex shader we can 
		 We can edit the vertex shader to swap the y-coordinate automatically
		  by replacing the TexCoord assignment with TexCoord = vec2(texCoord.x, 1.0f - texCoord.y);. 
	
	**************/
	TexCoord = vec2(texCoord.x, 1.0 - texCoord.y);
}

